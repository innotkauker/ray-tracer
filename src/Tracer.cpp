#include "Tracer.h"
#include "atlimage.h"
#include <omp.h>
#include <iostream>

using namespace glm;

SRay CTracer::MakeRay(unsigned x, unsigned y, unsigned w, unsigned h) 
{
	float xN = (x + 0.5f)/w * 2.0f - 1.0f;
	float yN = (y + 0.5f)/h * 2.0f - 1.0f;
	return SRay(m_camera.m_pos, normalize(m_camera.m_forward + Right_HalfPlane*xN + Up_HalfPlane*yN));
}

glm::vec3 CTracer::TraceRay(SRay ray) 
{
	return m_pScene->find_color(ray);
}

void CTracer::RenderImage()
{
	int width = m_camera.m_resolution.x;
	int height = m_camera.m_resolution.y;

	// m_camera.m_resolution = uvec2(width, height);
	m_camera.m_pixels.resize(width * height);
	Right_HalfPlane = m_camera.m_right * tan(m_camera.m_viewAngle.x/2);
	Up_HalfPlane = m_camera.m_up * tan(m_camera.m_viewAngle.y/2);

	unsigned total = height, cur = 0;
// #pragma omp parallel for num_threads(m_camera.n_threads)
	for(int i = 0; i < height; i++) {
		cur++;
		if (!(cur % 10))
			std::cout << cur << std::endl;
		for(int j = 0; j < width; j++) {
			SRay ray = MakeRay(j, i, width, height);
			m_pScene->set_depth(0);
			m_camera.m_pixels[i * width + j] = TraceRay(ray);
		}
	}
}

void CTracer::SaveImageToFile(std::string fileName) 
{
	CImage image;

	int width = m_camera.m_resolution.x;
	int height = m_camera.m_resolution.y;

	image.Create(width, height, 24);
		
	int pitch = image.GetPitch();
	unsigned char* imageBuffer = (unsigned char*)image.GetBits();

	if (pitch < 0) {
		imageBuffer += pitch * (height - 1);
		pitch =- pitch;
	}

	int i, j;
	int imageDisplacement = 0;
	int textureDisplacement = 0;

	for (i = 0; i < height; i++) {
		for (j = 0; j < width; j++) {
			vec3 color = m_camera.m_pixels[textureDisplacement + j];

			imageBuffer[imageDisplacement + j * 3] = clamp(color.b, 0.0f, 1.0f) * 255.0f;
			imageBuffer[imageDisplacement + j * 3 + 1] = clamp(color.g, 0.0f, 1.0f) * 255.0f;
			imageBuffer[imageDisplacement + j * 3 + 2] = clamp(color.r, 0.0f, 1.0f) * 255.0f;
		}

		imageDisplacement += pitch;
		textureDisplacement += width;
	}

	image.Save(fileName.c_str());
	image.Destroy();
}