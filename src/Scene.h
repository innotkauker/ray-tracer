#pragma once

#include "Types.h"
#include <vector>
#include "Object.h"

using std::vector;

class CScene {
	vector<Object *> objects;
	Light light;
	int depth;
	int max_depth;
	double cur_refraction;
private:
	float fresnel_reflectance(float ci, float n);
	vec3 reflect_ray(CollisionPoint &cp);
public:
	CScene(int objects_n = 10, int depth = 5, double refr = 1): 
					max_depth(depth),
					cur_refraction(refr) 
					{objects.reserve(objects_n);}
	void add_object(Object *src) {objects.push_back(src);}
	void set_light(Light &src) {light = src;}
	vec3 find_color(SRay &ray);
	void set_depth(int d) {depth = d;}
	vec3 return_color(CollisionPoint &cp);
};