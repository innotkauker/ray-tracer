#pragma once
#include "glm/glm.hpp"
#include "Types.h"

using glm::vec3;

typedef struct Material Material;
typedef struct Light Light;
typedef struct CollisionPoint CollisionPoint;

struct Material {
	vec3 ambient;
	vec3 diffuse;
	vec3 specular;
	vec3 emission;
	double shininess;
	vec3 color;
	double glass;
	double refractive;
	Material(vec3 a = vec3(0),
				vec3 d = vec3(0),
				vec3 s = vec3(0),
				vec3 e = vec3(0),
				double sh = 0,
				vec3 c = vec3(0),
				double g = 0,
				double r = 0): 
					ambient(a),
					diffuse(d),
					specular(s),
					emission(e),
					shininess(sh),
					color(c),
					glass(g),
					refractive(r) {}
};

struct Light {
	vec3 position;
	vec3 ambient;
	vec3 diffuse;
	vec3 specular;
	double attenuation;
	float power;
	Light(vec3 p = vec3(0), 
			vec3 a = vec3(0),
			vec3 d = vec3(0),
			vec3 s = vec3(0),
			double at = 0,
			float pow = 50): 
		position(p), 
		ambient(a), 
		diffuse(d), 
		specular(s), 
		attenuation(at),
		power(pow) {}
};

struct CollisionPoint {
	Material m;
	vec3 point;
	vec3 normal;
	SRay ray;
	float refr_coef;

	CollisionPoint() {}
	CollisionPoint(Material &src_m, vec3 &src_p, vec3 &src_n, SRay &src_ray): 
															m(src_m), 
															point(src_p), 
															normal(src_n),
															ray(src_ray) {}
};
