#include "Tracer.h"
#include "Object.h"

Light ceiling_light(
	vec3(0, 0, 80),
	vec3(0.5f, 0.5f, 0.5f),
	vec3(0.8f, 0.8f, 0.8f),
	vec3(0.0f, 0.0f, 0.0f),
	0.6f,
	50);

Material wall_material(
	vec3(0.8f, 0.8f, 0.8f),
	vec3(0.6f, 0.6f, 0.6f),
	vec3(0.1f, 0.1f, 0.1f),
	vec3(0, 0, 0),
	0.5f,
	vec3(0.6, 0.5, 0.8));

Material floor_material(
	vec3(0.8f, 0.8f, 0.8f),
	vec3(0.6f, 0.6f, 0.6f),
	vec3(0.3f, 0.3f, 0.3f),
	vec3(0, 0, 0),
	0.0f,
	vec3(0.8, 0.5, 0.1));

Material glass(
	vec3(0),
	vec3(0),
	vec3(0),
	vec3(0),
	0.0f,
	vec3(0),
	1.0);

Material crystal(
	vec3(0),
	vec3(0),
	vec3(0),
	vec3(0),
	0.0f,
	vec3(0),
	0.0,
	1.4);

void main()
{
	CTracer tracer(string("params"));
	CScene scene(10, tracer.m_camera.m_depth);
	Object *tmp;
	tmp = new Plane(vec3(-100, 0, 0), vec3(1, 0, 0));
	tmp->set_material(wall_material);
	scene.add_object(tmp);
	tmp = new Plane(vec3(0, -50, 0), vec3(0, 1, 0));
	tmp->set_material(glass);
	scene.add_object(tmp);
	tmp = new Plane(vec3(100, 0, 0), vec3(-1, 0, 0));
	tmp->set_material(wall_material);
	scene.add_object(tmp);
	tmp = new Plane(vec3(0, 50, 0), vec3(0, -1, 0));
	tmp->set_material(wall_material);
	scene.add_object(tmp);
	tmp = new Plane(vec3(0, 0, 0), vec3(0, 0, 1));
	tmp->set_material(floor_material);
	scene.add_object(tmp);
	tmp = new Plane(vec3(0, 0, 80), vec3(0, 0, -1));
	tmp->set_material(floor_material);
	scene.add_object(tmp);
	// tmp = new Plane(vec3(100, 50, 50), vec3(-1, -1, 1));
	// tmp->set_material(glass);
	// scene.add_object(tmp);

	tmp = new Model("Crystal.3DS", 90, vec3(0, 0, 7));
	tmp->set_material(crystal);
	scene.add_object(tmp);

	scene.set_light(ceiling_light);

	tracer.m_pScene = &scene;
	tracer.RenderImage();
	tracer.SaveImageToFile("Result.png");
}