#include "Object.h"
#include "glm/gtc/type_ptr.hpp"

// double operator*(vec3 a, vec3 b)
// {
// 	return a.x*b.x + a.y*b.y + a.z*b.z;
// }

vec3 convert(const LVector4 &src)
{
	return vec3(src.x, src.y, src.z);
}

double 
Plane::hit(SRay &ray, CollisionPoint &cp)
{
	float intersection_t = glm::dot(p - ray.m_start, n) / glm::dot(ray.m_dir, n);
	if (intersection_t > EPS) {
		cp = CollisionPoint(material, ray.m_start + intersection_t*ray.m_dir, n, ray);
		return intersection_t;
	} else {
		return 0;
	}
}

Model::Model(string src_file, float degree, vec3 &shift): o(src_file.c_str()) 
{
	// rotate the crystal
	glm::mat4x4 model_matrix = glm::translate(glm::mat4(), shift);
	model_matrix = glm::rotate(model_matrix, degree, vec3(1, 0, 0));
	LMatrix4 m;
	// here i recalled that i am a system programmer
	memcpy((void *)(m.m), (void *)(glm::value_ptr(model_matrix)), 16*sizeof(float));
	for (unsigned i = 0; i < o.GetMeshCount(); i++) {
		o.GetMesh(i).SetMatrix(m);
		o.GetMesh(i).Optimize(oNone); 
	}

}

double 
Model::hit(SRay &ray, CollisionPoint &cp)
{
	double min_t = 0;
	CollisionPoint nearest_cp;
	for (unsigned i = 0; i < o.GetMeshCount(); i++) {
		for (unsigned j = 0; j < o.GetMesh(i).GetTriangleCount(); j++) {
			// std::cout << scene.GetMesh(i).GetTriangle2(j).a << std::endl;
			CollisionPoint cur_cp;
			double cur_t = hit_triangle(ray, cur_cp, o.GetMesh(i), j);
			if (cur_t > EPS) { // hit!
				if (min_t < EPS || cur_t < min_t) { // we are closer then before
					min_t = cur_t;
					nearest_cp = cur_cp;
				}
			}
		}
	}
	nearest_cp.m = material;
	cp = nearest_cp; // fix this somehow
	return min_t;
}

double 
Model::hit_triangle(SRay &ray, CollisionPoint &cp, const LMesh &mesh, unsigned index)
{
	const LTriangle tr = mesh.GetTriangle(index);
	vec3 v0 = convert(mesh.GetVertex(tr.a)), 
		v1 = convert(mesh.GetVertex(tr.b)), 
		v2 = convert(mesh.GetVertex(tr.c));

	double a = v0.x - v1.x, b = v0.x - v2.x, c = ray.m_dir.x, d = v0.x - ray.m_start.x; 
	double e = v0.y - v1.y, f = v0.y - v2.y, g = ray.m_dir.y, h = v0.y - ray.m_start.y;
	double i = v0.z - v1.z, j = v0.z - v2.z, k = ray.m_dir.z, l = v0.z - ray.m_start.z;
		
	double m = f * k - g * j, n = h * k - g * l, p = f * l - h * j;
	double q = g * i - e * k, s = e * j - f * i;
	
	double inv_denom  = 1.0 / (a * m + b * q + c * s);
	
	double e1 = d * m - b * n - c * p;
	double beta = e1 * inv_denom;
	
	if (beta < 0.0)
	 	return 0;
	
	double r = r = e * l - h * i;
	double e2 = a * n + d * q + c * r;
	double gamma = e2 * inv_denom;
	
	if (gamma < 0.0 )
	 	return 0;
	
	if (beta + gamma > 1.0)
		return 0;
			
	double e3 = a * p - b * r + d * s;
	double t = e3 * inv_denom;
	
	if (t < EPS) 
		return 0;
					
	cp.normal = triangle_normal(v0, v1, v2);
	cp.point = ray.m_start + ray.m_dir * (float)t;	
	cp.ray = ray;
	return t;
}
