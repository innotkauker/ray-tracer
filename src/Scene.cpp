#include <iostream>
#include <algorithm>
#include "Scene.h"

#include "glm/gtx/extented_min_max.hpp"

using glm::normalize;
	
vec3 
CScene::find_color(SRay &ray) 
{
	if (depth > 1)
		std::clog << "depth = " << depth << std::endl;
	if (++depth > max_depth) {// decreade depth somewhere else and make stack not overflow
		depth--;
		return vec3(0);
	}
	double min_t = 0;
	CollisionPoint nearest_cp;
	for (unsigned i = 0; i < objects.size(); i++) {
		CollisionPoint cur_cp;
		double cur_t = objects[i]->hit(ray, cur_cp);
		if (cur_t > EPS) { // hit!
			if (min_t < EPS || cur_t < min_t) { // we are closer then before
				min_t = cur_t;
				nearest_cp = cur_cp;
			}
		}
	}
	depth--;
	if (min_t > EPS)
		return return_color(nearest_cp);
	else
		return vec3(0, 0, 0);
}

float 
CScene::fresnel_reflectance(float ci, float n)
{
	float ci2 = ci * ci;
	float si2 = 1.0f - ci2;
	float si4 = si2 * si2;
	float a = ci2 + n * n - 1.0f;
	float sqa = 2 * sqrtf(a) * ci;
	float b = ci2 + a;
	float c = ci2 * a + si4;
	float d = sqa * si2;
	return (b - sqa) / (b + sqa) * (1.0f + (c - d) / (c + d)) * 0.5f;
}

vec3
CScene::reflect_ray(CollisionPoint &cp)
{
	float refr_coef = cp.refr_coef/cur_refraction;
	vec3 n = cp.normal;
	vec3 r = -cp.ray.m_dir;
	float cos_i = glm::dot(r, cp.normal);
	float cos_t_2 = 1 - 1/refr_coef/refr_coef*(1 - cos_i*cos_i);
	if (cos_t_2 <= 0) // full internal reflection
		return vec3(0);
	cos_t_2 = sqrt(cos_t_2);
	return (1/refr_coef)*r - (cos_t_2 - 1/refr_coef*cos_i) * n;
}

vec3 
CScene::return_color(CollisionPoint &cp)
{
	// glass
	if (cp.m.glass > 0.001) {
		return find_color(SRay(cp.point, -glm::reflect(normalize(-cp.ray.m_dir), normalize(cp.normal))))*(float(cp.m.glass));
	}
	// crystal
	if (cp.m.refractive >= 1) {
		float R = fresnel_reflectance(glm::dot(-cp.ray.m_dir, cp.normal), cp.refr_coef/cur_refraction);
		// fix cur refr_coef btw
		vec3 reflected = find_color(SRay(cp.point, -glm::reflect(normalize(-cp.ray.m_dir), normalize(cp.normal)))) * R;
		vec3 refracted = vec3(0);
		if (R > 0.001) {
			double t = cur_refraction;
			cur_refraction = cp.refr_coef;
			vec3 reflected_ray = reflect_ray(cp);
			if (reflected_ray != vec3(0))
				refracted = find_color(SRay(cp.point, reflected_ray)) * (1 - R);
			cur_refraction = t;
		}
		return reflected + refracted;
	}

	// diffuse surfaces
	vec3 normal = normalize(cp.normal);
	vec3 view_direction = normalize(-cp.ray.m_dir);
	vec3 light_direction = light.position - cp.point;
	double distance = glm::length(light_direction);
	light_direction = normalize(light_direction);

	float attenuation = 1.0/(light.attenuation*distance);
	vec3 color_coefficient = cp.m.emission + cp.m.ambient * light.ambient * attenuation;
	float diffuse_coefficient = glm::max(double(glm::dot(normal, light_direction)), 0.0);
	color_coefficient += diffuse_coefficient * cp.m.diffuse * light.diffuse * attenuation;
	double specular_coefficient = (glm::dot(normal, -light_direction) < 0 ? std::max(pow(glm::max(glm::dot(glm::reflect(-light_direction, normal), view_direction), 0.0f), cp.m.shininess), 0.0) : 0);
	color_coefficient += cp.m.specular * light.specular * (float)specular_coefficient;
	return color_coefficient * cp.m.color * light.power;
}
