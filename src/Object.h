#pragma once
#include "glm/glm.hpp"
#include "CollisionPoint.h"
#include "l3ds.h"

using glm::vec3;

class Object {
protected:
	Material material;
public:
	Object() {}
	virtual ~Object() {}
	virtual double hit(SRay &ray, CollisionPoint &cp) = 0;
	void set_material(Material &src) {material = src;}
	Material get_material() {return material;}
};

class Plane: public Object {
private:
	vec3 p;
	vec3 n;
public:
	Plane(): p(0), n(0) {}
	Plane(vec3 &point, vec3 &normal): p(point), n(normal) {}
	~Plane() {}
	virtual double hit(SRay &ray, CollisionPoint &cp);
};

class Model: public Object {
	L3DS o;
private:
	double hit_triangle(SRay &ray, CollisionPoint &cp, const LMesh &m, unsigned index);
	inline vec3 triangle_normal(vec3 &v0, vec3 &v1, vec3 &v2) {
		return glm::normalize(glm::cross(v1 - v0, v2 - v0));
	}
public:
	Model(string src_file, float degree = 90.0f, vec3 &shift = vec3(0));
	~Model() {}
	double hit(SRay &ray, CollisionPoint &cp);
};