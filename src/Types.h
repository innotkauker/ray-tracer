#pragma once

#include "glm/glm.hpp"
#include "glm/gtc/matrix_transform.hpp"
#include "glm/gtx/rotate_vector.hpp"
#include "vector"
#include <fstream>
#include <cmath>
#include <string>

using glm::vec3;
using std::string;

#define PI 3.14159265
#define EPS 0.001

struct SRay {
	vec3 m_start;
	vec3 m_dir;
	SRay(vec3 &src_s = vec3(0, 0, 0), vec3 &src_d = vec3(0, 0, 0)) {m_start = src_s; m_dir = src_d;}
};

struct SCamera {
	vec3 m_pos;          // Camera position and orientation
	vec3 m_forward;      // Orthonormal basis
	vec3 m_right;
	vec3 m_up;

	glm::vec2 m_viewAngle;    // View angles, rad
	glm::uvec2 m_resolution;  // Image resolution: w, h
	unsigned m_depth;
	unsigned n_threads; // the camera can use multiple processors

	std::vector<vec3> m_pixels;  // Pixel array

	SCamera(string &file) {
		std::fstream src;
		src.open(file.c_str(), std::fstream::in);
		src >> m_pos.x;
		src >> m_pos.y;
		src >> m_pos.z;
		src >> m_forward.x;
		src >> m_forward.y;
		src >> m_forward.z;
		src >> m_viewAngle.x;
		src >> m_viewAngle.y;
		src >> m_resolution.x;
		src >> m_resolution.y;
		src >> m_depth;
		src >> n_threads;
		src.close();
		m_forward = glm::normalize(m_forward);
		double alpha = asin(m_forward.z);
		double phi = atan2(m_forward.y, m_forward.x);
		double radius = cos(PI/2 - alpha);
		m_up = glm::normalize(vec3(radius*cos(PI + phi), radius*sin(PI + phi), sin(PI/2 - alpha)));
		m_right = glm::normalize(glm::rotateZ(vec3(m_forward.x, m_forward.y, 0), float(-90)));
		// glm::mat4x4 view_matrix = glm::lookAt(m_pos, m_forward, m_up) * glm::vec4(1, 0, 0, 0);
		// m_right = vec3(glm::lookAt(m_pos, m_forward, m_up) * glm::vec4(0, 0, 1, 0));
	}
};

struct SMesh
{
	std::vector<vec3> m_vertices;  // vertex positions
	std::vector<glm::uvec3> m_triangles;  // vetrex indices
};